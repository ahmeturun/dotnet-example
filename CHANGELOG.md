# Changelog

All notable changes to this project will be documented in this file.

## 1.2.3.4

### Added

- Added these things

### Changed
- Changed a thing
- Changed another thing

## 1.1.1.1

### Added

- Added an old thing

## 1.0.0.0

### Added

- Added initial app