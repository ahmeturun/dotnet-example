using NUnit.Framework;

namespace ExampleLib.UnitTests
{
    public class Tests
    {
        [Test]
        public void TestAdding()
        {
            Assert.That(Calculator.Add(2, 2), Is.EqualTo(4));
        }

        public void TestMultiplying()
        {
            Assert.That(Calculator.Multiply(2, 2), Is.EqualTo(4));
        }

        public void TestDividing()
        {
            Assert.That(Calculator.Divide(10, 2), Is.EqualTo(5));
        }
    }
}